#!/usr/bin/python3

# SPDX-FileCopyrightText: 2020 Nicolás Alvarez <nicolas.alvarez@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Daemon that accepts connections via the git:// protocol
and simply returns an error message.
"""

import asyncio
import datetime
import sys

async def read_pktline(reader):
    chunk_size_data = await reader.read(4)
    chunk_size = int(chunk_size_data, 16)
    return await reader.read(chunk_size)

async def write_pktline(writer, data):
    chunk_size_data = ('%04x' % (len(data)+4)).encode('ascii')
    writer.write(chunk_size_data)
    writer.write(data)
    await writer.drain()

async def handle_connection(reader, writer):
    try:
        request = await read_pktline(reader)

        request = request.split(b'\0')[0]
        command, pathname = request.split(b' ')

        command = command.decode('utf-8')
        pathname = pathname.decode('utf-8')

        ip = writer.get_extra_info('peername')[0]
        timestamp = datetime.datetime.utcnow().isoformat()
        sys.stdout.write(f"{timestamp} {ip} \"{command} {pathname}\"\n")
        sys.stdout.flush()

        await write_pktline(writer, "ERR Please use the https: protocol to connect to anongit\n".encode('ascii'))
    finally:
        writer.close()

loop = asyncio.get_event_loop()
coro = asyncio.start_server(handle_connection, '0.0.0.0', 9418, loop=loop)

server = loop.run_until_complete(coro)

loop.run_forever()
