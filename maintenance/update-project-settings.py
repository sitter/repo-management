#!/usr/bin/python3
import os
import re
import sys
import yaml
import argparse
import gitlab

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Updates the Gitlab configuration based on repo-metadata')
parser.add_argument('--metadata-path', help='Path to the metadata to check', required=True)
parser.add_argument('--instance', help='Gitlab instance to work with', required=True)
parser.add_argument('--token', help='User token to use when authenticating with Gitlab', required=True)
args = parser.parse_args()

# Make sure our configuration file exists
if not os.path.exists( args.metadata_path ):
    print("Unable to locate specified metadata location: {}".format(args.metadata_path))
    sys.exit(1)

# Connect to Gitlab
gitlabServer = gitlab.Gitlab( args.instance, private_token=args.token )

# The following are the patterns we want to protect
branchPatternsToProtect = ['master', '*.*']

# Start going over the location in question...
for currentPath, subdirectories, filesInFolder in os.walk( args.metadata_path, topdown=False, followlinks=False ):
    # Do we have a metadata.yaml file?
    if 'metadata.yaml' not in filesInFolder:
        # We're not interested then....
        continue

    # Are we looking at a neon/ path?
    regexToMatch = os.path.join( args.metadata_path, 'neon', '.*' )
    if re.match( regexToMatch, currentPath ):
        # Then we are also not interested
        # These repositories are looked after by a separate script (update-neon-project-settings.py) which have different sets of rules around them
        continue

    # Now that we know we have something to work with....
    # Lets load the current metadata up
    metadataPath = os.path.join( currentPath, 'metadata.yaml' )
    metadataFile = open( metadataPath, 'r' )
    metadata = yaml.safe_load( metadataFile )

    # create gitlab project instance
    project = gitlabServer.projects.get(metadata['repopath'])

    # We do this in two rounds, first round is settings which should not fail
    # second round is cosmetic data such as name and description.

    # First we update the merge method setting
    project.merge_method = 'ff'
    # As well as the only allow merge if discussions are resolved setting
    project.only_allow_merge_if_all_discussions_are_resolved = True
    # Disable request access option for all projects, as we want access control managed by developer account process
    project.request_access_enabled = False

    # Save the first round of information
    project.save()

    # Now second round which is cosmetics.
    # check sanity of the description and make sure that it is less then 250
    if metadata['description'] is not None and (len(metadata['description']) > 250 or len(metadata['description']) == 0):
        print("Description is too long or empty for " + metadata['repopath'])
    else:
        project.description = metadata['description']

    # Set name and description
    project.name = metadata['name']

    # Finally save the settings
    try:
        project.save()
    except:
        print ("Error saving project " + metadata['repopath'])

    # Start a list of known protection rules
    knownRules = []

    # Go over the protection rules we have and make sure they conform with the above
    for protected in project.protectedbranches.list():
        # Is it one of the ones we're allowed to have?
        if protected.name not in branchPatternsToProtect:
            # As it isn't in the list, get rid of it
            protected.delete()
            # Then go on to the next one
            continue

        # Now we need to make sure the access levels are correct
        if protected.merge_access_levels[0]['access_level'] != gitlab.DEVELOPER_ACCESS or protected.push_access_levels[0]['access_level'] != gitlab.DEVELOPER_ACCESS:
            # Turns out the Gitlab API does not support updating protection rules (at least not with the Python module)
            # We therefore have to remove these ones as well
            protected.delete()
            # Now go on to the next one
            continue

        # Finally add it to the list of rules we've found so we can keep track
        knownRules.append( protected.name )

    # Now determine which ones we are missing
    missingRules = [ pattern for pattern in branchPatternsToProtect if pattern not in knownRules ]

    # Go over the ones we are missing
    for rule in missingRules:
        # And create them
        project.protectedbranches.create({
            'name': rule,
            'merge_access_level': gitlab.DEVELOPER_ACCESS,
            'push_access_level': gitlab.DEVELOPER_ACCESS
        })

# All done!
sys.exit(0)

